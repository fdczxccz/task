import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public final class SimpleDataTable implements DataTable<String, Long, Long> {
    private final List<String> columnNames;
    private final Map<Long, List<Long>> tableRepresentation;

    public SimpleDataTable(List<String> colNames, List<Long> values) {
        if (colNames == null || colNames.size() == 0) // may replace with notEmpty constraint
        {
            throw new IllegalArgumentException("Some msg");
        }
        if (values == null || values.size() == 0) // may replace with notEmpty constraint
        {
            throw new IllegalArgumentException("Some msg");
        }
        columnNames = new ArrayList<>(colNames);
        final AtomicLong counter = new AtomicLong();
        tableRepresentation = values.stream().collect(Collectors.groupingBy(it -> counter.getAndIncrement() / columnNames.size()));
    }

    @Override
    public List<Long> getRow(Long rowValue) {
        throw new NotImplementedException();
    }

    @Override
    public List<Long> getColumn(String column) {
        throw new NotImplementedException();
    }

    @Override
    public Optional<Long> getValue(Long rowNumber, String column) {
        int columnIndex = columnNames.indexOf(column);
        if (columnIndex < 0) {
            throw new IllegalArgumentException("Some msg");
        }
        return Optional.ofNullable(tableRepresentation.get(rowNumber))
                .map(row -> row.get(columnIndex)); // throw out of bound exception, definitely fits here
    }
}
