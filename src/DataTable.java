import java.util.List;
import java.util.Optional;

//*
// COL_TYPE, ROW_TYPE should implement equals and hashcode according to java rules
// */
public interface DataTable<COL_TYPE, ROW_TYPE, VALUE_TYPE>
{
    List<VALUE_TYPE> getRow(ROW_TYPE rowValue);

    List<VALUE_TYPE> getColumn(COL_TYPE column);

    Optional<VALUE_TYPE> getValue(ROW_TYPE row, COL_TYPE column);

}
