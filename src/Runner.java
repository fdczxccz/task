import java.text.DateFormatSymbols;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class Runner implements Runnable
{
    // Locale.getDefault for your lang localized month
    private static final List<String> monthNames = Arrays.asList(new DateFormatSymbols(Locale.ENGLISH).getShortMonths());

    @Override
    public void run()
    {
        final List<Long>  prices = Arrays.asList(2L, 5L, 8L, 16L, 20L, 2L, 5L, 8L, 16L, 20L, 2L, 9L, 3L, 16L, 20L, 2L, 5L, 8L);
        SimpleDataTable productPrices = createProductPricesDataTable(prices);
        final Long product = 1L;
        final String month = "Apr";
        System.out.println(productPrices.getValue(product, month).get());
    }

    public SimpleDataTable createProductPricesDataTable(List<Long> prices)
    {
        return new SimpleDataTable(monthNames, prices);
    }
}
